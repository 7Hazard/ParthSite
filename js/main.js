var aboutMeSubtits = [
    "Why would you care?",
    "Who cares?",
    "Not that you'd care or anything...",
    "I don't even know myself...",
    "Thats a good question..."
];

var projectsSubtits = [
    "I'm not productive",
    "I am very lazy you will come to find"
];

function SetSubtit(subtit){
    var oh2 = '<h2 class="subtitle">';
    var ch2 = '</h2>';
    var arr;
    switch (subtit){
        case 'AboutMe':
            arr = aboutMeSubtits;
            break;
        case 'Projects':
            arr = projectsSubtits;
            break;
    }
    $('#'+subtit+" .subtitle").append(oh2+arr[Random(arr.length)]+ch2);
}

document.addEventListener('DOMContentLoaded', function () {
    
      // Get all "navbar-burger" elements
      var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
    
      // Check if there are any navbar burgers
      if ($navbarBurgers.length > 0) {
    
        // Add a click event on each of them
        $navbarBurgers.forEach(function ($el) {
          $el.addEventListener('click', function () {
    
            // Get the target from the "data-target" attribute
            var target = $el.dataset.target;
            var $target = document.getElementById(target);
    
            // Toggle the class on both the "navbar-burger" and the "navbar-menu"
            $el.classList.toggle('is-active');
            $target.classList.toggle('is-active');
    
          });
        });
      }
    
    });

$(window).on( "load", function(){
    SetSubtit('AboutMe');
    SetSubtit('Projects');
    $("#preloader").addClass("animated fadeOut is-invisible")
});

function Random(max) {
  return Math.floor(Math.random() * Math.floor(max));
}